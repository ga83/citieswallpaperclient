package com.tux.cities;

public class ImageData {
    public String imageUrl;
    public String imageContextUrl;
    public String imageOwner;
    public String imageTitle;
    public String site;
    public String city;
    public String country;
    public int license;
}