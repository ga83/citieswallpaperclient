package com.tux.cities;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.BufferUtils;

import applogger.CitiesWallpaperLogger;
import applogger.GdxAppUsageLogger;
import applogger.ParametersNotSetException;


import java.nio.IntBuffer;


/*
** TODO: When program starts, it shows the previous image but does not have the ImageData anymore because it was only stored in memory.
** TODO: Fix this by storing ImageData in preferences or something.
*/

public class CitiesLiveWallpaper extends ApplicationAdapter implements InputProcessor {

	public static String IP = "";
	private static int DEFAULT_MILLISECONDS_BETWEEN_UPDATES = 60 * 60 * 1000;

	private static int FPS_NORMAL = 3;
	private static int FPS_DISPLAYING_OVERLAY = 60;

	private SpriteBatch sb;
	private WallpaperGetter wallpaperGetter;

	private long lastUpdate = Long.MIN_VALUE;

	private float[] positionsAndDimensions;
	public static int maxTextureSize;

	BitmapFont bitmapFont;
	private long lastScreenTouch = Long.MIN_VALUE;
	private long OVERLAY_TIME = 3 * 1000;
	private long lastSb = 0;
	public static boolean preferencesChanged;

	private int millisecondsBetweenUpdates = DEFAULT_MILLISECONDS_BETWEEN_UPDATES;

	private GdxAppUsageLogger appLogger;
	private String logUrl = "http://" + IP + "/logappusage.php";
	private String applicationName = "citieslivewallpaper";
	private int logInterval = 10; // seconds

	@Override public void resize(int width, int height) {
//		System.out.println("resized");
	}

	public void touchLastScreenTouch() {
		this.lastScreenTouch = System.currentTimeMillis();
	}

	@Override
	public void create () {
		sb = new SpriteBatch();
		this.wallpaperGetter = new WallpaperGetter(this);
		this.positionsAndDimensions = new float[4];
		maxTextureSize = getMaxTextureSize();
		bitmapFont = new BitmapFont();

		FreeTypeFontGenerator generator;
		generator = new FreeTypeFontGenerator(Gdx.files.internal("Ubuntu-B.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = (int) (15 * Gdx.graphics.getDensity());
		this.bitmapFont = generator.generateFont(parameter);
		generator.dispose();
		Gdx.input.setInputProcessor(this);
		loadPreferences();
		this.lastUpdate = System.currentTimeMillis() - (millisecondsBetweenUpdates + 1000);

		/*
		this.appLogger = new CitiesWallpaperLogger();
		this.appLogger.setInterval(logInterval);
		this.appLogger.setApplicationName(applicationName);
		this.appLogger.setUrl(logUrl);


		try {
			this.appLogger.log();
		} catch (ParametersNotSetException e) {
			e.printStackTrace();
		}
		*/

	}

	private static int getMaxTextureSize () {
		IntBuffer buffer = BufferUtils.newIntBuffer(16);
		Gdx.gl.glGetIntegerv(GL20.GL_MAX_TEXTURE_SIZE, buffer);
		return buffer.get(0);
	}

	private void loadPreferences() {
		Preferences prefs = Gdx.app.getPreferences("prefs");

		if(prefs.getString("minutesBetweenUpdates") == "") {
			prefs.putString("minutesBetweenUpdates","60");
		}

		prefs.flush();
		prefs = Gdx.app.getPreferences("prefs");

		this.millisecondsBetweenUpdates = Integer.valueOf(prefs.getString("minutesBetweenUpdates")) * 60 * 1000;
	}

	@Override
	public void render () {

		if(preferencesChanged == true) {
			loadPreferences();
			preferencesChanged = false;
		}

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		long currentTime = System.currentTimeMillis();

		if(currentTime - lastSb > 2000) {
			lastSb = currentTime;
			sb.dispose();
			sb = new SpriteBatch();
		}


		if(
				(this.lastUpdate < currentTime - millisecondsBetweenUpdates || this.wallpaperGetter.getCurrentWallpaper() == null) &&
				this.wallpaperGetter.waitingForUrlOrBinary() == false
				) {
			this.wallpaperGetter.getWallpaperUrl();
		}

		sb.begin();

		Texture wallpaper = this.wallpaperGetter.getCurrentWallpaper();

		if(wallpaper != null) {
			getCorners(wallpaper);
			sb.setColor(1,1,1,1);
			sb.draw(new TextureRegion(wallpaper), positionsAndDimensions[0], positionsAndDimensions[1], positionsAndDimensions[2], positionsAndDimensions[3]);
			drawOverlay();
		}

		sb.end();

		// change this to a scheme of exiting the render method before glclear() if time hasn't reached next frame update
		try {
			if(displayingOverlay() == true) {
				Thread.sleep(1000 / FPS_DISPLAYING_OVERLAY);
			}
			else {
				Thread.sleep(1000 / FPS_NORMAL);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	private boolean displayingOverlay() {
		return this.lastScreenTouch > System.currentTimeMillis() - (OVERLAY_TIME + 1000);
	}

	private void drawOverlay() {
		if(displayingOverlay() == true) {
			long currentTime = System.currentTimeMillis();
			long timeSinceTouch = (currentTime + 1) - lastScreenTouch;

			float alpha;
			if(timeSinceTouch < OVERLAY_TIME * 0.9f) {
				alpha = 1f;
			}
			else {
				long fadeLength = (long) (OVERLAY_TIME * 0.1);
				long beginningOfFade = (long) (lastScreenTouch + OVERLAY_TIME * 0.9);
				long timeSinceBeginningOfFade = currentTime - beginningOfFade;
				alpha = 1f - timeSinceBeginningOfFade / (float)fadeLength;
			}

			bitmapFont.setColor(0, 0, 0, alpha);
			bitmapFont.draw(sb, wallpaperGetter.getOverlay(), Gdx.graphics.getWidth() / 20, Gdx.graphics.getHeight() / 4);
			bitmapFont.setColor(1, 1, 1, alpha);
			bitmapFont.draw(sb, wallpaperGetter.getOverlay(), Gdx.graphics.getWidth() / 20 - 3, Gdx.graphics.getHeight() / 4 + 3);
		}
	}

	private void getCorners(Texture wallpaper) {
	//	this.positionsAndDimensions[] = { x1,y1,w,h }

		float wallpaperWidth = wallpaper.getWidth();
		float wallpaperHeight = wallpaper.getHeight();

		float screenCentreX = Gdx.graphics.getWidth() / 2;
		float screenCentreY = Gdx.graphics.getHeight() / 2;

		float screenWidth = Gdx.graphics.getWidth();
		float screenHeight = Gdx.graphics.getHeight();

		if(wallpaperWidth > screenWidth && wallpaperHeight < screenHeight) {
			scaleHeightCropWidth(wallpaperWidth, wallpaperHeight, screenHeight, screenCentreX);
		}
		else if(wallpaperWidth < screenWidth && wallpaperHeight > screenHeight) {
			scaleWidthCropHeight(wallpaperWidth, wallpaperHeight, screenWidth, screenCentreY);
		}
		else if(wallpaperWidth > screenWidth && wallpaperHeight > screenHeight) {
			float widthRatio = wallpaperWidth / screenWidth;
			float heightRatio = wallpaperHeight / screenHeight;

			if(widthRatio < heightRatio) {
				scaleWidthCropHeight(wallpaperWidth, wallpaperHeight, screenWidth, screenCentreY);
			}
			else {
				scaleHeightCropWidth(wallpaperWidth, wallpaperHeight, screenHeight, screenCentreX);
			}
		}
		else if(wallpaperWidth < screenWidth && wallpaperHeight < screenHeight) {
			float widthRatio = wallpaperWidth / screenWidth;
			float heightRatio = wallpaperHeight / screenHeight;

			if(widthRatio < heightRatio) {
				scaleWidthCropHeight(wallpaperWidth, wallpaperHeight, screenWidth, screenCentreY);
			}
			else {
				scaleHeightCropWidth(wallpaperWidth, wallpaperHeight, screenHeight, screenCentreX);
			}
		}
	}

	private void scaleHeightCropWidth(float wallpaperWidth,float wallpaperHeight,float screenHeight, float screenCentreX) {
		float scale = screenHeight / wallpaperHeight;
		wallpaperWidth *= scale;
		this.positionsAndDimensions[1] = 0;
		this.positionsAndDimensions[3] = screenHeight;
		this.positionsAndDimensions[0] = screenCentreX - (wallpaperWidth / 2f);
		this.positionsAndDimensions[2] = wallpaperWidth;
	}

	private void scaleWidthCropHeight(float wallpaperWidth, float wallpaperHeight, float screenWidth, float screenCentreY) {
		float scale = screenWidth / wallpaperWidth;
		wallpaperHeight *= scale;
		this.positionsAndDimensions[0] = 0;
		this.positionsAndDimensions[2] = screenWidth;
		this.positionsAndDimensions[1] = screenCentreY - (wallpaperHeight / 2f);
		this.positionsAndDimensions[3] = wallpaperHeight;
	}

	@Override
	public void dispose () {
		sb.dispose();
	}

	public void touchLastUpdateTime() {
//		this.lastUpdate = System.currentTimeMillis();

		// catch up
		while(this.lastUpdate + millisecondsBetweenUpdates <= System.currentTimeMillis()) {
			this.lastUpdate += millisecondsBetweenUpdates;
//			System.out.println("catching up");
		}
//		System.out.println("caught up");
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		this.touchLastScreenTouch();
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
