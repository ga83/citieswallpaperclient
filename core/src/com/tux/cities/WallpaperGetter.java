package com.tux.cities;

import com.badlogic.gdx.graphics.Texture;

/**
 * Created by ubuntu on 2/07/18.
 */


public class WallpaperGetter  {

    private Texture wallpaperCurrent;	// soon, we will need both landscape and portraits stored, also devices can be rotated any time.
    private Object wallpaperCurrentLock;

    private UrlGetter urlGetter;
    private boolean waitingForBinary = false;
    private boolean waitingForUrl = false;
    private ImageData imageData;
    private ImageData imageDataWaiting = null;


    public WallpaperGetter(CitiesLiveWallpaper citiesLiveWallpaper) {
        this.wallpaperCurrentLock = new Object();
        this.urlGetter = new UrlGetter(new ImageGetter(this,citiesLiveWallpaper),this);
 //       this.wallpaperCurrent = new Texture("loading.png");

//        System.out.println("calling loadimage");
        ImageGetter.loadImageFromFile(this,citiesLiveWallpaper,true);
    }


    public void getWallpaperUrl() {
//        System.out.println("geturl");
        this.urlGetter.getUrl();
    }

    public Texture getCurrentWallpaper() {
            return this.wallpaperCurrent;
    }

    public void setWallpaper(Texture wallpaper) {
            if(this.wallpaperCurrent != null) {
                this.wallpaperCurrent.dispose();
            }
            this.wallpaperCurrent = wallpaper;
    }

    public void setWaitingForBinary(boolean waiting) {
            this.waitingForBinary = waiting;
    }

    public void setWaitingForUrl(boolean waiting) {
            this.waitingForUrl = waiting;
    }

    public boolean waitingForUrlOrBinary() {
            return (this.waitingForBinary | this.waitingForUrl);
    }

    public void setImageDataWaiting(ImageData imageData) {
        this.imageDataWaiting = imageData;
    }

    public void updateImageData() {
        this.imageData = this.imageDataWaiting;
    }

    public String getOverlay() {
        if(this.imageData != null) {
            return
                    this.imageData.city + "\n" + imageData.country + "\n" +
                    this.imageData.imageOwner;
        }
        else {
            return "";
        }
    }
}
