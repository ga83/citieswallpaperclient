package com.tux.cities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.badlogic.gdx.utils.Json;

/**
 * Created by ubuntu on 2/07/18.
 */

public class UrlGetter implements Net.HttpResponseListener {

    private final ImageGetter imageGetter;
    private final WallpaperGetter wallpaperGetter;

    public UrlGetter(ImageGetter imageGetter, WallpaperGetter wallpaperGetter) {
        this.imageGetter = imageGetter;
        this.wallpaperGetter = wallpaperGetter;
    }

    @Override
    public void handleHttpResponse(Net.HttpResponse httpResponse) {

        String responseString = httpResponse.getResultAsString();
        System.out.println("geturl handleresponse " + responseString);

        try {
            ImageData imageData = parseHttpResponse(responseString);
            String imageUrl = imageData.imageUrl;
            setImageDataWaiting(imageData);
            this.wallpaperGetter.setWaitingForUrl(false);
            this.imageGetter.getWallpaperBinary(imageUrl);
        }
        catch(Exception e) {
            System.out.println("url handler ex: " + e.getMessage());
            this.failed(e);
        }
    }

    private void setImageDataWaiting(final ImageData imageData) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                wallpaperGetter.setImageDataWaiting(imageData);
            }
        });
    }

    public void getUrl() {
        HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();
        Net.HttpRequest request = requestBuilder.newRequest().method(Net.HttpMethods.GET).url("http://" + CitiesLiveWallpaper.IP + "/randomphoto.php?width=" + width + "&height=" + height).build();
        request.setTimeOut(1000 * 20);
        Gdx.net.sendHttpRequest(request,this);
        this.wallpaperGetter.setWaitingForUrl(true);
    }

    private ImageData parseHttpResponse(String response) {
        Json json = new Json();
        ImageData imageData = json.fromJson(ImageData.class, response);
        return imageData;
    }

    @Override
    public void failed(Throwable t) {
        System.out.println("geturl failed " + t.getMessage());
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                wallpaperGetter.setWaitingForUrl(false);
                wallpaperGetter.setWaitingForBinary(false);
            }
        });
    }

    @Override
    public void cancelled() {
        System.out.println("geturl cancelled");
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                wallpaperGetter.setWaitingForUrl(false);
                wallpaperGetter.setWaitingForBinary(false);
            }
        });
    }

}


