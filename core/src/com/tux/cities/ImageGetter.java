package com.tux.cities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.tux.cities.CitiesLiveWallpaper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by ubuntu on 2/07/18.
 */

public class ImageGetter implements Net.HttpResponseListener {

    static String FILENAME = "image_cached";
    private CitiesLiveWallpaper citiesLiveWallpaper;
    WallpaperGetter wallpaperGetter;


    public ImageGetter(WallpaperGetter wallpaperGetter, CitiesLiveWallpaper citiesLiveWallpaper) {
        this.citiesLiveWallpaper = citiesLiveWallpaper;
        this.wallpaperGetter = wallpaperGetter;
    }

    @Override
    public void handleHttpResponse(Net.HttpResponse httpResponse) {

//        System.out.println("getbinary handleresponse");
        long length = Long.parseLong(httpResponse.getHeader("Content-Length"));
        System.out.println(length + " bytes to download");
        InputStream is = httpResponse.getResultAsStream();
        OutputStream os = Gdx.files.local(FILENAME).write(false);

        byte[] bytes = new byte[1024];
        int count = -1;
        long read = 0;
        boolean failed = false;

        try {
            while ((count = is.read(bytes, 0, bytes.length)) != -1) {
                os.write(bytes, 0, count);
                read += count;
            }

        } catch (IOException e) {
            System.out.println("ioe: " + e.getMessage());
            failed = true;
            this.failed(e);
        }

        if(failed == false) {
            loadImageFromFile(wallpaperGetter,citiesLiveWallpaper,false);
        }
    }

    public static void loadImageFromFile(final WallpaperGetter wallpaperGetter, final CitiesLiveWallpaper citiesLiveWallpaper, final boolean initialLoad) {

        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {

                Pixmap pixmapFromFile = null;

                try {
                    pixmapFromFile = new Pixmap(Gdx.files.local(FILENAME));
                }
                catch(Exception e) {
                    System.out.println(e.getMessage());
                    wallpaperGetter.setWallpaper(new Texture("loading.png"));
                    wallpaperGetter.setWaitingForBinary(false);
                    return;
                }
                Pixmap pixmapResized = null;

                int fileWidth = pixmapFromFile.getWidth();
                int fileHeight = pixmapFromFile.getHeight();

                if (
                        fileWidth > CitiesLiveWallpaper.maxTextureSize |
                                fileHeight > CitiesLiveWallpaper.maxTextureSize
                        ) {
                    int pixmapWidth = fileWidth;
                    int pixmapHeight = fileHeight;

                    while (pixmapWidth > CitiesLiveWallpaper.maxTextureSize | pixmapHeight > CitiesLiveWallpaper.maxTextureSize) {
                        pixmapWidth /= 2;
                        pixmapHeight /= 2;
                    }

                    pixmapResized = new Pixmap(pixmapWidth, pixmapHeight, Pixmap.Format.RGBA8888);
                    pixmapResized.drawPixmap(pixmapFromFile, 0, 0, fileWidth, fileHeight, 0, 0, pixmapWidth, pixmapHeight);
                    pixmapFromFile.dispose();
                } else {
                    pixmapResized = pixmapFromFile;
                }
                final Texture wallpaper = new Texture(pixmapResized);

                pixmapResized.dispose();

                wallpaperGetter.setWallpaper(wallpaper);
                wallpaperGetter.setWaitingForBinary(false);
                wallpaperGetter.updateImageData();

                if(initialLoad == false) {
                    citiesLiveWallpaper.touchLastUpdateTime();
                }
                citiesLiveWallpaper.touchLastScreenTouch();
            }
        });

    }

    public void getWallpaperBinary(String imageUrl) {
//        System.out.println("getbinary");
        HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
        Net.HttpRequest request = requestBuilder.newRequest().method(Net.HttpMethods.GET).url(imageUrl).build();
        request.setTimeOut(20 * 1000);
        Gdx.net.sendHttpRequest(request,this);
        this.wallpaperGetter.setWaitingForBinary(true);
    }

    @Override
    public void failed(Throwable t) {
        System.out.println("getbinary failed " + t.getMessage());
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                wallpaperGetter.setWaitingForUrl(false);
                wallpaperGetter.setWaitingForBinary(false);
            }
        });
    }

    @Override
    public void cancelled() {
        System.out.println("getbinary cancelled");
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                wallpaperGetter.setWaitingForUrl(false);
                wallpaperGetter.setWaitingForBinary(false);
            }
        });
    }
}
