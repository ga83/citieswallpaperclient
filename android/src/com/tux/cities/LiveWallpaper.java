/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.tux.cities;

import android.annotation.TargetApi;
import android.content.res.Configuration;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService;
import com.badlogic.gdx.backends.android.AndroidWallpaperListener;

import android.os.Build;
import android.security.NetworkSecurityPolicy;


public class LiveWallpaper extends AndroidLiveWallpaperService {
	
	@TargetApi(Build.VERSION_CODES.M)
	@Override
	public void onCreateApplication () {
		super.onCreateApplication();
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		ApplicationListener listener = new MyLiveWallpaperListener();
		initialize(listener, config);

		android.security.NetworkSecurityPolicy ab = android.security.NetworkSecurityPolicy.getInstance();

//		System.out.println("aaa " + ab.isCleartextTrafficPermitted());
	}

	@Override
	public void onConfigurationChanged(Configuration config) {
//		System.out.println("config changed");
		super.onConfigurationChanged(config);
	}


	// implement AndroidWallpaperListener additionally to ApplicationListener 
	// if you want to receive callbacks specific to live wallpapers
	public static class MyLiveWallpaperListener extends CitiesLiveWallpaper implements AndroidWallpaperListener {

		@Override public void resize(int width, int height) {
//			System.out.println("resized");
		}



		@Override
		public void offsetChange (float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset,
			int yPixelOffset) {
//			Log.i("LiveWallpaper test", "offsetChange(xOffset:"+xOffset+" yOffset:"+yOffset+" xOffsetSteep:"+xOffsetStep+" yOffsetStep:"+yOffsetStep+" xPixelOffset:"+xPixelOffset+" yPixelOffset:"+yPixelOffset+")");
		}

		@Override
		public void previewStateChange (boolean isPreview) {
//			Log.i("LiveWallpaper test", "previewStateChange(isPreview:"+isPreview+")");
		}

		@Override
		public void iconDropped (int x, int y) {
//			Log.i("LiveWallpaper test", "iconDropped (" + x + ", " + y + ")");
		}
		
		
	}
}