package com.tux.cities;


import android.graphics.Color;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

public class CitiesPreferencesActivity extends PreferenceActivity {


    @Override
    public void onResume()
    {
        super.onResume();

        getPreferenceManager().setSharedPreferencesName("prefs");
        addPreferencesFromResource(R.xml.prefs);

        Preference circlePreference = getPreferenceScreen().findPreference(
                "minutesBetweenUpdates");

        circlePreference.setOnPreferenceChangeListener(numberCheckListener);

        getListView().setBackgroundColor(Color.argb(192,0,0,0));
    }

/*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName("prefs");
        addPreferencesFromResource(R.xml.prefs);

        Preference circlePreference = getPreferenceScreen().findPreference(
                "minutesBetweenUpdates");

        circlePreference.setOnPreferenceChangeListener(numberCheckListener);

//        getPreferenceManager().setSharedPreferencesMode(0);
    }
*/



    Preference.OnPreferenceChangeListener numberCheckListener = new Preference.OnPreferenceChangeListener() {

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {

            // check that the string is an integer
             if (newValue != null && newValue.toString().length() > 0
                     && newValue.toString().matches("\\d*"))
            {
                CitiesLiveWallpaper.preferencesChanged = true;
                return true;
            }

             return false;
        }

    };

}